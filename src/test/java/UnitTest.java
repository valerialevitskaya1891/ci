
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class UnitTest {

    @Test
    //Addition
    public void testAddition() {
        String input = "2 + 3";
        double expected = 5.0;

        double result = evaluateExpression(input);
        Assertions.assertEquals(expected, result);
    }

    @Test
    //Subtraction
    public void testSubtraction() {
        String input = "5 - 3";
        double expected = 2.0;

        double result = evaluateExpression(input);
        Assertions.assertEquals(expected, result);
    }

    @Test
    //Multiplication
    public void testMultiplication() {
        String input = "2 * 4";
        double expected = 8.0;

        double result = evaluateExpression(input);
        Assertions.assertEquals(expected, result);
    }

    @Test
    //Division
    public void testDivision() {
        String input = "10 / 5";
        double expected = 2.0;

        double result = evaluateExpression(input);
        Assertions.assertEquals(expected, result);
    }

    @Test
// Division by zero test
    public void testDivisionByZero() {
        String input = "5 / 0";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            evaluateExpression(input);
        }, "Деление на ноль недопустимо.");
    }


    @Test
// Searching for warnings
    public void testInvalidInput() {
        String input = "2 + 3 *";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            evaluateExpression(input);
        }, "Некорректный формат числа.");
    }


    @Test
    //Searching for warnings
    public void testUnsupportedOperation() {
        String input = "2 ^ 3";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            evaluateExpression(input);
        });
    }

    private double evaluateExpression(String input) {
        //Execution
        ConsoleCalculator calculator = new ConsoleCalculator();
        return calculator.evaluateExpression(input);
    }
}
