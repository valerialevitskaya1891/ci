import java.io.File;
import java.io.FilenameFilter;
import org.junit.jupiter.api.Test;

public class FunctionalTest {
    @Test

    public void FunctionalTest() {
        String projectRootPath = System.getProperty("user.dir");
        String targetFolderPath = projectRootPath + "/target";
        String jarFileExtension = ".jar";

        File targetFolder = new File(targetFolderPath);

        File[] jarFiles = targetFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(jarFileExtension);
            }
        });

        if (jarFiles != null && jarFiles.length > 0) {
            System.out.println("Файлы .jar существуют в папке target:");
            for (File file : jarFiles) {
                System.out.println(file.getName());
            }
        } else {
            System.out.println("Файлы .jar не найдены в папке target.");
        }
    }
}
