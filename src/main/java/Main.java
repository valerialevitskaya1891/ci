// (с) Copyright В.Р. Левицкая, А.А. Ломатов, 2023
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ConsoleCalculator calculator = new ConsoleCalculator();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите выражение вида 5 - 4 * 3: ");
        String input = scanner.nextLine();

        double result = calculator.evaluateExpression(input);
        System.out.println("Результат: " + result);

        scanner.close();
    }
}
