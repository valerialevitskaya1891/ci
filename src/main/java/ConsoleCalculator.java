// (с) Copyright В.Р. Левицкая, А.А. Ломатов, 2023
public class ConsoleCalculator {
    public double evaluateExpression(String input) {
        String[] tokens = input.split(" ");
        if (tokens.length % 2 == 0) {
            throw new IllegalArgumentException("Некорректный формат ввода.");
        }

        double result = Double.parseDouble(tokens[0]);

        String operator = null;
        for (int i = 1; i < tokens.length; i += 2) {
            operator = tokens[i];
            double operand;

            try {
                operand = Double.parseDouble(tokens[i + 1]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Некорректный формат числа.");
            }

           switch (operator) {
               case "+":
                   result += operand;
                   break;
               case "-":
                   result -= operand;
                   break;
               case "*":
                   result *= operand;
                   break;
               case "/":
                   if (operand == 0) {
                       throw new IllegalArgumentException("Деление на ноль недопустимо.");
                   }
                   result /= operand;
                   break;
               default:
                   throw new IllegalArgumentException("Неподдерживаемая операция.");
           }

       }

        return result;
    }
}